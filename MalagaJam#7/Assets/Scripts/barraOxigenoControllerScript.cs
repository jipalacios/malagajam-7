﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barraOxigenoControllerScript : MonoBehaviour {

	public GameObject[] puntosOxigeno;
	public float tiempoOxigeno, incrementoOxigeno, tiempoEsperaIncremento, perdidaOxigeno;
	private float inicioContadorOxigeno,tiempoPorcionOxigeno, tiempoRestanteOxigeno, ultimoIncrementoHecho = 0.0f;
	private int porcionesRestantesOxigeno;
	private GameObject levelManager;
	// Use this for initialization
	void Start () {
		inicioContadorOxigeno = Time.time;
		tiempoPorcionOxigeno = tiempoOxigeno / 12.0f;
		levelManager = GameObject.FindGameObjectWithTag("LevelManager");
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (Time.time > inicioContadorOxigeno + tiempoOxigeno) {
			levelManager.GetComponent<LevelManager>().LoadLevel("GameOver");
		}
		tiempoRestanteOxigeno = (inicioContadorOxigeno + tiempoOxigeno) - Time.time;
		porcionesRestantesOxigeno = 12 - (int)((tiempoOxigeno - tiempoRestanteOxigeno)/tiempoPorcionOxigeno);
		pintaBarraOxigeno ();
	}

	public void incrementaOxigeno(){
		if (Time.time > ultimoIncrementoHecho + tiempoEsperaIncremento) {
			if (inicioContadorOxigeno + incrementoOxigeno > Time.time) {
				inicioContadorOxigeno = Time.time;
				ultimoIncrementoHecho = Time.time;
			} else {
				inicioContadorOxigeno = inicioContadorOxigeno + incrementoOxigeno;
				ultimoIncrementoHecho = Time.time;
			}
		}
	}

	public void pierdeOxigeno(){
		inicioContadorOxigeno -= perdidaOxigeno;
	}


	void pintaBarraOxigeno(){
		puntosOxigeno [10].SetActive (false);
		puntosOxigeno [9].SetActive (false);
		puntosOxigeno [8].SetActive (false);
		puntosOxigeno [7].SetActive (false);
		puntosOxigeno [6].SetActive (false);
		puntosOxigeno [5].SetActive (false);
		puntosOxigeno [4].SetActive (false);
		puntosOxigeno [3].SetActive (false);
		puntosOxigeno [2].SetActive (false);
		puntosOxigeno [1].SetActive (false);
		puntosOxigeno [0].SetActive (false);
		switch (porcionesRestantesOxigeno) {
		case 1:
			puntosOxigeno [0].SetActive (true);
			break;
		case 2:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			break;
		case 3:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			break;
		case 4:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			break;
		case 5:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			break;
		case 6:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			break;
		case 7:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			puntosOxigeno [6].SetActive (true);
			break;
		case 8:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			puntosOxigeno [6].SetActive (true);
			puntosOxigeno [7].SetActive (true);
			break;
		case 9:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			puntosOxigeno [6].SetActive (true);
			puntosOxigeno [7].SetActive (true);
			puntosOxigeno [8].SetActive (true);
			break;
		case 10:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			puntosOxigeno [6].SetActive (true);
			puntosOxigeno [7].SetActive (true);
			puntosOxigeno [8].SetActive (true);
			puntosOxigeno [9].SetActive (true);
			break;
		case 11:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			puntosOxigeno [6].SetActive (true);
			puntosOxigeno [7].SetActive (true);
			puntosOxigeno [8].SetActive (true);
			puntosOxigeno [9].SetActive (true);
			puntosOxigeno [10].SetActive (true);
			break;
		case 12:
			puntosOxigeno [0].SetActive (true);
			puntosOxigeno [1].SetActive (true);
			puntosOxigeno [2].SetActive (true);
			puntosOxigeno [3].SetActive (true);
			puntosOxigeno [4].SetActive (true);
			puntosOxigeno [5].SetActive (true);
			puntosOxigeno [6].SetActive (true);
			puntosOxigeno [7].SetActive (true);
			puntosOxigeno [8].SetActive (true);
			puntosOxigeno [9].SetActive (true);
			puntosOxigeno [10].SetActive (true);
			break;
		}
	}
}
