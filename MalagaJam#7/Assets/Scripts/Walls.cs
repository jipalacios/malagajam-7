﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Walls : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D (Collider2D col){
		if (transform.tag == "end") {
			if (col.transform.tag == "mob") {
				Destroy (col.gameObject);
			}
		}
		if (transform.tag == "top") {
			if (col.transform.tag == "mob") {
				col.GetComponent<Mob_movement> ().goingDown = true;
			}
		}
	}
}
