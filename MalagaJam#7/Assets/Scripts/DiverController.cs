﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DiverController : MonoBehaviour {
	public float smoothTime = 40f;
	public float delay = 1f;
	private GameObject target;
	private Vector3 targetPosition;
	private Vector3 velocity = Vector3.zero;

	// Use this for initialization
	void Start () {
		target = GameObject.FindGameObjectWithTag ("Player");
		
	}
	// Update is called once per frame
	void FixedUpdate () {
		//targetPosition = new Vector3 (target.transform.position.x, target.transform.position.y - target.GetComponent<ShipController> ().rope, 0);
		//transform.position = Vector3.SmoothDamp (transform.position, targetPosition, ref velocity, smoothTime * Time.deltaTime);
	}

	void OnTriggerEnter2D (Collider2D col){
		if (col.transform.tag == "PezBueno") {
			GameObject.FindObjectOfType<ContadorPeces>().contador-=1;
			GameObject goodFish = GameObject.FindGameObjectWithTag ("PezBueno");

			int fishPosition = Random.Range (0, 3);
			if (fishPosition == 0) {
				// Spawning on right
				goodFish.GetComponent<Transform>().position = GameObject.FindGameObjectWithTag ("spawnDerecha").GetComponent<Transform>().position;
				goodFish.GetComponent<Mob_movement> ().speedX = Random.Range (-2.0F, -0.5F);
				goodFish.GetComponent<Mob_movement> ().speedY = Random.Range (-2.0F, -0.5F);
				//newInstance.Play();
			} else if (fishPosition == 1) {
				// Spawning on left
				goodFish.GetComponent<Transform>().position = GameObject.FindGameObjectWithTag ("spawnIzquierda").GetComponent<Transform>().position;
				goodFish.GetComponent<Mob_movement> ().speedX = Random.Range (2.0F, 0.5F);
				goodFish.GetComponent<Mob_movement> ().speedY = Random.Range (-2.0F, -0.5F);
			} else {
				// Spawning Bottom
				goodFish.GetComponent<Transform>().position = GameObject.FindGameObjectWithTag ("spawnInferior").GetComponent<Transform>().position;
				goodFish.GetComponent<Mob_movement> ().speedX = Random.Range (-1.5F, 1.5F);
				goodFish.GetComponent<Mob_movement> ().speedY = Random.Range (0.5F, 2.0F);
			}

			if (goodFish.GetComponent<Mob_movement> ().speedX > 0) {
				goodFish.gameObject.GetComponent<SpriteRenderer> ().flipX = true;
			}
		}

		if (col.transform.tag == "Pez") {
			GameObject.FindObjectOfType<barraOxigenoControllerScript>().pierdeOxigeno ();
		}
	}
}