﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class inputBarcoControllerScript : MonoBehaviour {
	public bool adelante;
	public float valorInput;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnMouseDown(){
		if (adelante) {
			GameObject.FindObjectOfType<BoatControllerScript> ().inputMovimiento = valorInput;
		} else {
			GameObject.FindObjectOfType<BoatControllerScript> ().inputMovimiento = valorInput *-1.0f;
		}
	}	

	void OnMouseUp(){
		GameObject.FindObjectOfType<BoatControllerScript> ().inputMovimiento = 0.0f;
	}
}
