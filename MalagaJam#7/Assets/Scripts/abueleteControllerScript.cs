﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class abueleteControllerScript : MonoBehaviour
{

	public GameObject[] posiciones;
	public float velocidadAbuelete;
	private float orientacionAbuelete = 0.0f;
	public GameObject objetoModo;
	// 0: Cabina
	// 1: Bomba
	// 2: Polea
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (transform.position.x - objetoModo.transform.position.x >= 0.2f || transform.position.x - objetoModo.transform.position.x <= -0.2f) {
			GameObject.FindObjectOfType<modoBarcoControllerScript> ().modo = modoBarco.cambiandoModo;
			transform.Translate (orientacionAbuelete * (velocidadAbuelete / 100.0f), 0.0f, 0.0f);
		} else {
			switch (objetoModo.GetComponent<clicObjetoScript> ().tipo) {
			case tipoObjeto.Bomba:
				GameObject.FindObjectOfType<modoBarcoControllerScript> ().modo = modoBarco.modoOxigeno;
				break;
			case tipoObjeto.Cabina:
				GameObject.FindObjectOfType<modoBarcoControllerScript> ().modo = modoBarco.modoConduccion;
				break;
			case tipoObjeto.Polea:
				GameObject.FindObjectOfType<modoBarcoControllerScript> ().modo = modoBarco.modoPolea;
				break;
			}
		}
	}

	public void cambiarModoA (GameObject g)
	{
		objetoModo = g;
		if ((objetoModo.transform.position.x - transform.position.x) < 0) {
			orientacionAbuelete = -1.0f;
		} else {
			orientacionAbuelete = 1.0f;
		}
	}
}