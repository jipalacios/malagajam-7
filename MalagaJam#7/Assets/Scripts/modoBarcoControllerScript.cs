﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum modoBarco{
	cambiandoModo,
	modoConduccion,
	modoOxigeno,
	modoPolea
}

public class modoBarcoControllerScript : MonoBehaviour {

	public modoBarco modo;
	public GameObject[] interfacesBarco;

	// Use this for initialization
	void Start () {
		modo = modoBarco.cambiandoModo;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		switch (modo) {
		case modoBarco.cambiandoModo:
			interfacesBarco[0].SetActive(false);
			interfacesBarco[1].SetActive(false);
			interfacesBarco[2].SetActive(false);
			break;
		case modoBarco.modoConduccion:
			interfacesBarco[1].SetActive(true);
			break;
		case modoBarco.modoOxigeno:
			interfacesBarco[0].SetActive(true);
			break;
		case modoBarco.modoPolea:
			interfacesBarco[2].SetActive(true);
			break;
		}
		//Debug.Log (modo);		
	}

	void OnMouseDown(){
	}
}
