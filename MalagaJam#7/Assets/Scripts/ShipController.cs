﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController : MonoBehaviour {

	public float rope = 3f;
	// Use this for initialization
	void Start () {
	}
	// Update is called once per frame
	void Update () {
		if (Input.GetKey (KeyCode.DownArrow)) {
			rope += 0.01f;
		} else if (Input.GetKey (KeyCode.UpArrow)) {
			if (rope > 0f) {	
				rope -= 0.01f;
			} else {
				rope = 0f;
			}
		}
	}
}
