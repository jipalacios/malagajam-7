﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Olas : MonoBehaviour {
	public List<GameObject> olas;
	public GameObject olaPrefab;
	private GameObject newOla;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void OnTriggerEnter2D (Collider2D col){
		if (col.transform.tag == "ola") {
			SpawnOla ();
		}
	}
	void SpawnOla(){
		newOla = Instantiate (olaPrefab, olas [0].transform.GetChild (0).position, transform.rotation);
	}
}
