﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music_Player : MonoBehaviour {
	static Music_Player music_player_OBJ = null;

	void Awake () {
		if (music_player_OBJ != null) {
			Destroy (gameObject);
		} else {
			music_player_OBJ = this;
			GameObject.DontDestroyOnLoad (gameObject);
		}
		
	}
	// Use this for initialization
	void Start () {
		//AudioSource().volume = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (transform.GetComponent<AudioSource> ().time < 0.10F) {
			transform.GetComponent<AudioSource> ().volume = 0;
		} else if (transform.GetComponent<AudioSource> ().time >= 0.10F && transform.GetComponent<AudioSource> ().time < 5F) {
			transform.GetComponent<AudioSource> ().volume += Time.deltaTime * 0.9F;
		} 
	}
}
