﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum estadoBarco
{
	Parado,
	Acelerando,
	Movimiento,
	Frenando
}

public class BoatControllerScript : MonoBehaviour
{

	// Use this for initialization
	public estadoBarco estado;
	public float inputMovimiento, velocidad, velocidadAcelerando, velocidadFrenando, tiempoAcelerando, tiempoFrenando, bajadaCuerdaPorGiro, tiempoEsperaGiro, limiteBuzoArriba, limiteBuzoAbajo, limiteBarcoDerecha, limiteBarcoIzquierda;
	public float limiteTopAnimacion, limiteBotAnimacion, velocidadAnimacion, subidaOBajada;
	public GameObject posicionBuzo;
	private float inicioAceleracion, inicioFrenado, direccionFrenadoBarco, ultimoGiroHecho;

	void Start ()
	{
		estado = estadoBarco.Parado;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (transform.position.y >= limiteTopAnimacion) {
			subidaOBajada = -1.0f;
		}else{
			if(transform.position.y <= limiteBotAnimacion)
				subidaOBajada = 1.0f;
		}


		if (inputMovimiento != 0.0f) {
			if (estado == estadoBarco.Parado) {
				if (inputMovimiento > 0.0f) {
					direccionFrenadoBarco = 1.0f;
				} else {
					direccionFrenadoBarco = -1.0f;
				}
				estado = estadoBarco.Acelerando;
				inicioAceleracion = Time.time;
			} else {
				if (estado == estadoBarco.Acelerando) {
					if (Time.time > inicioAceleracion + tiempoAcelerando) {
						estado = estadoBarco.Movimiento;
					}
				}
			}
		} else {
			if (estado == estadoBarco.Acelerando || estado == estadoBarco.Movimiento) {
				estado = estadoBarco.Frenando;
				inicioFrenado = Time.time;
			}
		}

		if(direccionFrenadoBarco < 0.0f && transform.position.x < limiteBarcoIzquierda){
			transform.position = new Vector3 (limiteBarcoIzquierda,transform.position.y,transform.position.z);
		}
		if (direccionFrenadoBarco > 0.0f && transform.position.x > limiteBarcoDerecha) {
			transform.position = new Vector3 (limiteBarcoDerecha,transform.position.y,transform.position.z);
		} 

		switch (estado) {
		case estadoBarco.Acelerando:
			transform.Translate (inputMovimiento * (velocidadAcelerando / 100.0f), velocidadAnimacion * subidaOBajada, 0.0f);
			break;
		case estadoBarco.Frenando:
			transform.Translate (direccionFrenadoBarco * (velocidadFrenando / 100.0f),velocidadAnimacion * subidaOBajada, 0.0f);
			if (Time.time > inicioFrenado + tiempoFrenando) {
				estado = estadoBarco.Parado;
			}
			break;
		case estadoBarco.Movimiento:
		case estadoBarco.Parado:
			transform.Translate (inputMovimiento * (velocidad / 100.0f), velocidadAnimacion * subidaOBajada, 0.0f);
			break;
		}
	}

	public void giroPolea(bool derecha){
		if (Time.time > ultimoGiroHecho + tiempoEsperaGiro) {
			ultimoGiroHecho = Time.time;
			if (derecha) {
				if (Vector3.Distance (posicionBuzo.transform.position, transform.position) < limiteBuzoAbajo)
					posicionBuzo.transform.position = new Vector3 (posicionBuzo.transform.position.x, posicionBuzo.transform.position.y - bajadaCuerdaPorGiro, posicionBuzo.transform.position.z);
			} else {
				if (Vector3.Distance (posicionBuzo.transform.position, transform.position) > limiteBuzoArriba)
					posicionBuzo.transform.position = new Vector3 (posicionBuzo.transform.position.x, posicionBuzo.transform.position.y + bajadaCuerdaPorGiro, posicionBuzo.transform.position.z);			
			}
		}
	}
}
