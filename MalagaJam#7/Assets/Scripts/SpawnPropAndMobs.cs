﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPropAndMobs : MonoBehaviour {
	public int id;
	public GameObject[] instances;
	public GameObject newInstance;
	public GameObject fishType;
	public float spawnTimer = 2f;
	public float timeLimit;
	private int fishPosition;
	private static Random.State seedGenerator;
	public bool pezBueno;
	// Use this for initialization
	void Start () {
		timeLimit = 1;
		seedGenerator = Random.state;

		fishType = instances [0];
		pezBueno = true;
		fishPosition = Random.Range (2, 3);
		if (fishPosition == 0) {
			// Spawning on right
			GameObject spawnPoint = GameObject.FindGameObjectWithTag ("spawnDerecha");
			newInstance = Instantiate (fishType, new Vector3 (spawnPoint.transform.position.x, Random.Range (-5, 1), 0), transform.rotation);
			newInstance.GetComponent<Mob_movement> ().speedX = Random.Range (-2.0F, -0.5F);
			newInstance.GetComponent<Mob_movement> ().speedY = Random.Range (-2.0F, -0.5F);
			//newInstance.Play();
		} else if (fishPosition == 1) {
			// Spawning on left
			GameObject spawnPoint = GameObject.FindGameObjectWithTag ("spawnIzquierda");
			newInstance = Instantiate (fishType, new Vector3 (spawnPoint.transform.position.x, Random.Range (5, 1), 0), transform.rotation);
			newInstance.GetComponent<Mob_movement> ().speedX = 1;//Random.Range (1, 4);
			newInstance.GetComponent<Mob_movement> ().speedY = 1;//Random.Range (-3, -3);
		} else {
			// Spawning Bottom
			GameObject spawnPoint = GameObject.FindGameObjectWithTag ("spawnInferior");
			newInstance = Instantiate (fishType, new Vector3 (Random.Range (-9, 9), spawnPoint.transform.position.y, 0), transform.rotation);
			newInstance.GetComponent<Mob_movement> ().speedX = Random.Range (-1.5F, 1.5F);
			newInstance.GetComponent<Mob_movement> ().speedY = Random.Range (0.5F, 2.0F);
		}

		if (newInstance.GetComponent<Mob_movement> ().speedX > 0) {
			newInstance.gameObject.GetComponent<SpriteRenderer> ().flipX = true;
		}
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		timeLimit += Time.deltaTime;
		if (timeLimit > spawnTimer) {
			timeLimit = 0f;
			if (pezBueno) {
				fishType = instances [Random.Range (1, 3)];
			} else {
				fishType = instances [Random.Range (0, 3)];
			}
			fishPosition = Random.Range (2, 3);
			if (fishPosition == 0) {
				// Spawning on right
				GameObject spawnPoint = GameObject.FindGameObjectWithTag ("spawnDerecha");
				newInstance = Instantiate (fishType, new Vector3 (spawnPoint.transform.position.x, Random.Range (-5, 1), 0), transform.rotation);
				newInstance.GetComponent<Mob_movement> ().speedX = Random.Range (-4, -1);
				newInstance.GetComponent<Mob_movement> ().speedY = Random.Range (-3, 3);
				pezBueno = true;
				//newInstance.Play();
			} else if (fishPosition == 1) {
				// Spawning on left
				GameObject spawnPoint = GameObject.FindGameObjectWithTag ("spawnIzquierda");
				newInstance = Instantiate (fishType, new Vector3 (spawnPoint.transform.position.x, Random.Range (5, 1), 0), transform.rotation);
				newInstance.GetComponent<Mob_movement> ().speedX = 3;//Random.Range (1, 4);
				newInstance.GetComponent<Mob_movement> ().speedY = 0;//Random.Range (-3, -3);
			} else {
				// Spawning Bottom
				GameObject spawnPoint = GameObject.FindGameObjectWithTag ("spawnInferior");
				newInstance = Instantiate (fishType, new Vector3 (Random.Range (-9, 9), spawnPoint.transform.position.y, 0), transform.rotation);
				newInstance.GetComponent<Mob_movement> ().speedX = Random.Range (-3, 3);
				newInstance.GetComponent<Mob_movement> ().speedY = Random.Range (1, 4);
			}

			if (newInstance.GetComponent<Mob_movement> ().speedX > 0) {
				newInstance.gameObject.GetComponent<SpriteRenderer> ().flipX = true;
			}

		}
	}
}
