﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mob_movement : MonoBehaviour {
	public int id;
	public float speedX, speedY;
	public bool goingDown;
	private GameObject goodFish;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		transform.position =  transform.position + new Vector3 (speedX, speedY, 0) * Time.fixedDeltaTime;

		/*if (id == 1) {
			transform.Translate (Vector3.left* Time.deltaTime * speed);
		} else if (id == 2) {
			transform.Translate (Vector3.right * Time.deltaTime * speed);
		} else {
			if (!goingDown) {
				transform.Translate (Vector3.up * Time.deltaTime * speed);
			} else {
				transform.Translate (Vector3.down * Time.deltaTime * speed);
			}
		}*/
	}
	void OnTriggerEnter2D(Collider2D collision){
		if (tag == "PezBueno") {
			if (collision.gameObject.tag == "LimiteH") {
				goodFish = GameObject.FindGameObjectWithTag ("PezBueno");
				goodFish.gameObject.GetComponent<Mob_movement> ().speedX = -goodFish.gameObject.GetComponent<Mob_movement> ().speedX;
				goodFish.gameObject.GetComponent<SpriteRenderer> ().flipX = !goodFish.gameObject.GetComponent<SpriteRenderer> ().flipX;
			} else if (collision.gameObject.tag == "LimiteV") {
				goodFish = GameObject.FindGameObjectWithTag ("PezBueno");
				goodFish.gameObject.GetComponent<Mob_movement> ().speedY = -goodFish.gameObject.GetComponent<Mob_movement> ().speedY;
				goodFish.gameObject.GetComponent<SpriteRenderer> ().flipY = !goodFish.gameObject.GetComponent<SpriteRenderer> ().flipY;
			}
		} else if (collision.gameObject.tag == "LimiteH" || collision.gameObject.tag == "LimiteV") {
			Destroy (gameObject);
		}
	}
}
