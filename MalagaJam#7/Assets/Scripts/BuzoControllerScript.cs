﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuzoControllerScript : MonoBehaviour
{

	public GameObject posicion;
	public float velocidadAcercamiento, velocidadBuzo, radioMovimiento;
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (Vector3.Distance (transform.position, posicion.transform.position) > radioMovimiento) {
			transform.position = Vector3.MoveTowards (transform.position, posicion.transform.position, velocidadAcercamiento);
		} else {
			transform.Translate (Input.GetAxis ("Horizontal") * (velocidadBuzo / 100.0f), Input.GetAxis ("Vertical") * (velocidadBuzo / 100.0f), 0.0f);
		}
	}
}
