﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ContadorPeces : MonoBehaviour {
	public Text texto;
	public int contador;
	public Transform pezMini;
	// Use this for initialization
	void Start () {
		contador = 15;
		pezMini = GameObject.Find ("Pez_mini").transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		texto.text = contador.ToString();

		if (contador <= 0) {
			GameObject.FindObjectOfType<LevelManager> ().LoadLevel ("Terminado");
		}
	}
}
